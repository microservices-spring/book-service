package com.example.books;

import com.example.books.entities.BookEntity;
import com.example.books.repositories.BookRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
public class data implements CommandLineRunner {

    @Autowired
    BookRepository bookRepository;

    @Override
    public void run(String... args) {

//        this.bookRepository.deleteAll();

        this.bookRepository.save(BookEntity.builder().bookName("the night").idStudent(99).bookCost(12.60).build());
        this.bookRepository.save(BookEntity.builder().bookName("Ahmed Mera").idStudent(1).bookCost(98.08).build());
        this.bookRepository.save(BookEntity.builder().bookName("fucking book").idStudent(99).bookCost(100.00).build());

    }
}
