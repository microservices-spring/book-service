package com.example.books.controllers;

import com.example.books.beans.Book;
import com.example.books.entities.BookEntity;
import com.example.books.repositories.BookRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/book")
@Slf4j
public class BookRestController {

    @Autowired
    Environment environment;

    @Autowired
    BookRepository bookRepository;

    @GetMapping("/data")
    public String getBookData() {

        log.debug("call getBookData");

        return "data of BOOK-SERVICE, Running on port: "
                + environment.getProperty("local.server.port");
    }

    @GetMapping("/{id}")
    public List<BookEntity> getBookById(@PathVariable int id) {
        log.debug("call getBookById - id {}", id);
        return this.bookRepository.findBookEntityByIdStudent(id);
    }

    @GetMapping("/all")
    public List<BookEntity> getAll(){
        log.debug("call getAll");

        return this.bookRepository.findAll();
    }

    @GetMapping("/entity")
    public ResponseEntity<String> getEntityData() {
        log.debug("call getEntityData");

        return new ResponseEntity<>(
                "Hello from BookRestController",
                HttpStatus.OK);
    }
}
