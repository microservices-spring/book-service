package com.example.books;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;


//@SpringBootApplication(exclude = {
//        org.springframework.cloud.client.serviceregistry.ServiceRegistryAutoConfiguration.class,
//        org.springframework.cloud.client.serviceregistry.AutoServiceRegistrationAutoConfiguration.class
//})
//@EnableEurekaClient
@EnableDiscoveryClient
@SpringBootApplication
public class BooksApplication {

    public static void main(String[] args) {
        SpringApplication.run(BooksApplication.class, args);
    }

}
