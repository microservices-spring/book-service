package com.example.books.entities;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serial;
import java.io.Serializable;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Document
public class BookEntity implements Serializable {
    @Serial
    private static final long serialVersionUID = 2440907763303955972L;

    @Id
    private String id;
    private String bookName;
    private Double bookCost;
    private int idStudent;
}
