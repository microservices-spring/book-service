package com.example.books.repositories;

import com.example.books.entities.BookEntity;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BookRepository extends MongoRepository<BookEntity, String> {
    @Query("{'IdStudent': ?0}")
    List<BookEntity> findBookEntityByIdStudent(int idStudent);
    List<BookEntity> findBookEntitiesByBookName(String name);
    List<BookEntity> findAll();
}
